# Our Portfolio with Drupal

## Requeriments

- Docker
- Docker-compose
- Makefile
- PHP 7.4 or superior installed

## Clone and Setup the Project

1) Go to the ulr (<https://gitlab.com/LilianePaixao/drupal-portfolio-dev>) and clone the project in your desktop

2) Make a copy of the `docker-compose.override.example.yml` and `.env.example` files and configure it in the way that you need in your local environment.

    *Change the .env settings as per your docker-compose.override.yml settings*

        DRUPAL_DATABASE_NAME=db_name
        DRUPAL_DATABASE_USERNAME=DRUPAL_DATABASE_USERNAME
        DRUPAL_DATABASE_PASSWORD=DRUPAL_DATABASE_PASSWORD
        DRUPAL_DATABASE_PREFIX=DRUPAL_DATABASE_PREFIX

    - Tip: If you need, you can change others configurations of your `docker.compose.override.yml` and `.env` as per your environment

3) In the terminal, run the command:

    `docker-compose up`

4) Go to the container with the command:

    `docker-compose exec web bash`

    And run the command:

    `composer install`