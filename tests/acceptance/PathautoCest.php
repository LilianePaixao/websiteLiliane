<?php

class PathautoCest

{
    public function BasicPage(AcceptanceTester $I)
    {
        $date = new DateTime();
        //go to login url - localhost/user/login
        $I->amOnPage('/user/login');
        
        //fill field username - name
        $I->fillField('name','lili_tcc');

        //fill field password - name
        $I->fillField('pass','123456789');

        //click on login button - value 
        $I->click('Log in', '#user-login-form');
        
        //click on Content - value 
        $I->click('Content', '#toolbar-item-administration-tray');
        
        //click on addContent 
        $I->click('Add content', '#block-claro-local-actions');
        
        //click on Basic Page 
        $I->click('Basic page', '#block-claro-content');
        
        //fillField title
        $I->fillField('title[0][value]','teste');
       
        //click on button save - and inner text are searched
        $I->click('Save', '#edit-actions' );

        //check the message created
        $I->see('Basic page teste has been created.');   
        
        //click on home
        $I->click('Home', '#block__content' );

        //go to verify the content
        $I->amOnPage('/admin/content');

        //click on teste
        $I->click('teste'.$date, '#views-field views-field-title' );

        //check the url created
        $I->see('/teste'.$date);
           
    }
    
}
